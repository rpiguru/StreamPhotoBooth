import gc
import traceback

from kivy.app import App
import config_kivy
import widgets.factory_reg

from kivy.core.window import Window
from kivy.uix.vkeyboard import VKeyboard
from kivy.lang import Builder
from kivy.base import ExceptionHandler, ExceptionManager
from kivy.clock import Clock
from kivy.input.providers.mouse import MouseMotionEvent
from kivy.properties import ObjectProperty, NumericProperty
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.screenmanager import SlideTransition, NoTransition

from kivymd.theming import ThemeManager
from screens.screen_manager import screens
from settings import INIT_SCREEN
from utils.camera_service import WebcamStreamer
import utils.logger as logger
from utils.common import is_landscape


# Needed to remove red circles from right/mid mouse buttons clicks
MouseMotionEvent.update_graphics = lambda *args: None


class SPBExceptionHandler(ExceptionHandler):
    """
    Exception Handler class to catch any exception and to show the error screen
    """

    def handle_exception(self, exception):
        """
        Catch the exception and show the error screen
        :param exception:
        :return:
        """
        logger.exception(exception)
        _app = App.get_running_app()
        # Save the traceback of the current exception.
        _app.root.save_exception(traceback.format_exc(limit=20))
        _app.root.switch_screen('error_screen')
        return ExceptionManager.PASS


ExceptionManager.add_handler(SPBExceptionHandler())


Builder.load_file('main.kv')


class StreamPhotoBoothWidget(FloatLayout):

    current_screen = None
    exception = None
    capacity = NumericProperty(0)

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.app = App.get_running_app()
        Clock.schedule_once(lambda dt: self._initialize())

    def _initialize(self):
        self.ids.video.start()
        self.switch_screen(INIT_SCREEN)

    def switch_screen(self, screen_name, direction=None):
        """
        Switch to the other screen
        :param screen_name: destination screen name
        :param direction: direction of transition
        :return:
        """
        sm = self.ids.sm
        if sm.has_screen(screen_name):  # Already in the destination screen?
            sm.current = screen_name
        else:
            screen = screens[screen_name](name=screen_name)
            if direction:
                sm.transition = SlideTransition(direction=direction)
            else:
                sm.transition = NoTransition()

            sm.switch_to(screen)

            logger.info(f'=== Switched to {screen_name} screen')

            # Delete the old screen
            if self.current_screen:
                sm.remove_widget(self.current_screen)
                del self.current_screen
                gc.collect()
            self.current_screen = screen

    def set_video_blur(self, is_blur):
        self.ids.video.is_blur = is_blur

    def flash_screen(self, duration=.2):
        self.capacity = .8
        Clock.schedule_once(lambda dt: self._remove_flash(), duration)

    def _remove_flash(self):
        self.capacity = 0

    def save_exception(self, ex):
        """
        This function is called by the Exception Handler.
        :param ex:
        :return:
        """
        self.exception = ex

    def get_exception(self):
        return self.exception


class SPBKeyboard(VKeyboard):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.layout_path = 'assets/layout'

    def setup_mode_dock(self, *largs):
        # Customized this function to use the VKeyboard of "Fixed" size.
        self.do_translation = False
        self.do_rotation = False
        self.do_scale = False
        self.rotation = 0
        win = self.get_parent_window()
        self.scale = 1.5
        self.pos = (win.width * (.75 if is_landscape else 1) - self.width * self.scale) / 2, 0
        win.bind(on_resize=self._update_dock_mode)


class StreamPhotoBoothApp(App):

    theme_cls = ThemeManager()
    camera = ObjectProperty()

    def build(self):
        Window.set_vkeyboard_class(SPBKeyboard)
        self.title = 'StreamPhotoBooth Application'
        self.camera = WebcamStreamer(video_port=0)
        self.camera.start()
        self.root = StreamPhotoBoothWidget()
        return self.root

    def get_video_frame(self):
        return self.camera.read()

    def take_snap_photo(self, template=True):
        return self.camera.create_snap_photo(template=template)

    def compose_gif(self):
        self.camera.compose_gif()

    def on_stop(self):
        self.camera.stop()
        super(StreamPhotoBoothApp, self).on_stop()


if __name__ == '__main__':

    app = StreamPhotoBoothApp()

    app.run()
