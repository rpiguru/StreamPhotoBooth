import threading
import time

from kivy.clock import Clock
from kivy.lang import Builder
from kivy.uix.video import Video

from screens.base import BaseScreen
from settings import INIT_SCREEN, EMAIL_SCREEN_TIMEOUT
from utils import logger
from utils.email_util import send_email, is_valid_email
from utils.store_selectors import RenderedImagePath, CaptureType
from widgets.dialog import LoadingDialog
from widgets.video import VideoFileWidget

Builder.load_file('screens/email_screen.kv')


class EmailScreen(BaseScreen):

    loading_dlg = None
    path = None

    def __init__(self, **kwargs):
        self._last_active_time = time.time()
        super().__init__(**kwargs)
        self._video_widget = None

    def on_enter(self, *args):
        self.loading_dlg = LoadingDialog()
        self.loading_dlg.open()
        Clock.schedule_interval(lambda dt: self._check_result(), .1)
        self.ids.txt_email.bind(text=self._on_txt_input)
        super(EmailScreen, self).on_enter(*args)
        Clock.schedule_interval(lambda dt: self._check_inactivity(), 1)

    def _check_result(self):
        self.path = RenderedImagePath().get()
        if self.path is not None:
            self.ids.card.disabled = False
            if CaptureType().get() != 'gif':
                self.ids.img.source = self.path
                self.ids.img.opacity = 1
            else:
                self.ids.box.clear_widgets()
                self._video_widget = VideoFileWidget(path=self.path)
                self.ids.box.add_widget(self._video_widget)
            self.loading_dlg.dismiss()
            return False

    def on_btn_send(self):
        email = self.ids.txt_email.text
        if is_valid_email(email):
            threading.Thread(
                target=send_email,
                args=("from_email@example.com", email, "Stream Photo Booth", "Please check attached", self.path)
            ).start()
            self.switch_screen('result_screen')
        else:
            self.ids.btn_send.source = 'assets/images/btn_email_bad.png'
            self.ids.img_caption.source = 'assets/images/email_label_bad.png'

    def _on_txt_input(self, *args):
        if 'bad' in self.ids.btn_send.source:
            self.ids.btn_send.source = 'assets/images/btn_email_good.png'
            self.ids.img_caption.source = 'assets/images/email_label_good.png'
        self._last_active_time = time.time()

    def on_btn_retake(self):
        RenderedImagePath().clear()
        self.switch_screen('capture_screen')

    def on_btn_start_over(self):
        self.switch_screen(INIT_SCREEN)

    def _check_inactivity(self):
        if time.time() - self._last_active_time > EMAIL_SCREEN_TIMEOUT * 60:
            logger.info("Returning to home screen after the timeout")
            self.switch_screen(INIT_SCREEN)

    def on_pre_leave(self, *args):
        super(EmailScreen, self).on_pre_leave(*args)
        if self._video_widget:
            self._video_widget.stop()
