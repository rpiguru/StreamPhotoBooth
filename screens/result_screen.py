from kivy.clock import Clock
from kivy.lang import Builder
from kivy.properties import BooleanProperty

from screens.base import BaseScreen


Builder.load_file('screens/result_screen.kv')


class ResultScreen(BaseScreen):

    def on_enter(self, *args):
        super(ResultScreen, self).on_enter(*args)
        Clock.schedule_once(lambda dt: self._update_image(), 3)

    def _update_image(self):
        self.ids.img.source = 'assets/images/check_your_email.png'
        Clock.schedule_once(lambda dt: self._return_home(), 3)

    def _return_home(self):
        self.switch_screen('home_screen')
