from kivy.app import App
from kivy.properties import ObjectProperty, BooleanProperty
from kivy.uix.screenmanager import Screen


class BaseScreen(Screen):

    app = ObjectProperty()
    blur_video = BooleanProperty(True)

    def __init__(self, **kwargs):
        self.app = App.get_running_app()
        super().__init__(**kwargs)

    def on_enter(self, *args):
        self.app.root.set_video_blur(self.blur_video)
        super(BaseScreen, self).on_enter(*args)

    def switch_screen(self, screen_name, direction=None):
        self.app.root.switch_screen(screen_name, direction)
