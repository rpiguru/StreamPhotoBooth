from kivy.uix.screenmanager import ScreenManager

from screens.capture_screen import CaptureScreen
from screens.email_screen import EmailScreen
from screens.error_screen import ErrorScreen
from screens.home_screen import HomeScreen
from screens.result_screen import ResultScreen


screens = {
    'home_screen': HomeScreen,
    'capture_screen': CaptureScreen,
    'email_screen': EmailScreen,
    'result_screen': ResultScreen,
    'error_screen': ErrorScreen,
}


sm = ScreenManager()
