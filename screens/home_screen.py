from kivy.lang import Builder
from kivy.properties import BooleanProperty

from screens.base import BaseScreen
from utils.store_selectors import CaptureType, clear_store

Builder.load_file('screens/home_screen.kv')


class HomeScreen(BaseScreen):

    blur_video = BooleanProperty(True)

    def on_enter(self, *args):
        clear_store()
        super(HomeScreen, self).on_enter(*args)

    def on_btn_start(self, capture_type):
        CaptureType().set(capture_type)
        self.switch_screen('capture_screen')
