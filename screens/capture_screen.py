import os
import datetime
import cv2
from kivy.animation import Animation
from kivy.clock import Clock
from kivy.lang import Builder
from kivy.properties import NumericProperty, ListProperty

from screens.base import BaseScreen
from settings import IMAGE_DIR
from utils.common import HEIGHT
from utils.cv_utils import compose_burst_image
from utils.store_selectors import CaptureType, RenderedImagePath


Builder.load_file('screens/capture_screen.kv')


class CaptureScreen(BaseScreen):

    cnt = NumericProperty(3)
    burst_cnt = NumericProperty(2)

    _burst_images = ListProperty()

    def on_enter(self, *args):
        super(CaptureScreen, self).on_enter(*args)
        Clock.schedule_once(lambda dt: self._start_counter(), 3)

    def _start_counter(self):
        img = self.ids.img_instruction
        img.y = HEIGHT
        capture_type = CaptureType().get()
        self.app.root.set_video_blur(False)
        if self.cnt > 0:
            img.source = f"assets/images/cnt_{self.cnt}.png"
            self.cnt -= 1
            anim = Animation(y=HEIGHT / 2 - img.height / 2, duration=.5,
                             t="out_expo") + Animation(y=-img.height, duration=.3, t="in_quad")
            anim.start(img)
            Clock.schedule_once(lambda dt: self._start_counter(), 1)
        else:
            if capture_type == 'snap':
                self.app.root.flash_screen(duration=.5)
            elif capture_type == 'gif':
                self.app.root.flash_screen()
                self.app.compose_gif()
            elif capture_type == 'burst':
                self.app.root.flash_screen()
                img_path = self.app.take_snap_photo(template=False)
                self._burst_images.append(cv2.imread(img_path))
                if self.burst_cnt > 0:
                    self.burst_cnt -= 1
                    self.cnt = 3
                    Clock.schedule_once(lambda dt: self._start_counter(), 1)
                    return

            Clock.schedule_once(lambda dt: self._on_finished_counter(), .5)

    def _on_finished_counter(self):
        capture_type = CaptureType().get()
        if capture_type == 'snap':
            img_path = self.app.take_snap_photo()
            RenderedImagePath().set(img_path)
            delay = 0
        elif capture_type == 'burst':
            img = compose_burst_image(self._burst_images)
            file_name = os.path.join(IMAGE_DIR, f"{datetime.datetime.now().isoformat().replace(':', '-')}.jpg")
            cv2.imwrite(file_name, img)
            RenderedImagePath().set(file_name)
            delay = 0
        else:
            delay = 1
        Clock.schedule_once(lambda dt: self.switch_screen('email_screen'), delay)
