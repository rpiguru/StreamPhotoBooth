import os

DEFAULT_WIDTH = 1920
DEFAULT_HEIGHT = 1080

CAMERA_WIDTH = 1920
CAMERA_HEIGHT = 1080

DARK_SCALE = .3
BLUR_SIZE = 25
GIF_DURATION = 1.5

BASE_DIR = os.path.expanduser('~/.spb/')
os.makedirs(BASE_DIR, exist_ok=True)
CONFIG_FILE = os.path.join(BASE_DIR, 'config.json')

IMAGE_DIR = os.path.join(BASE_DIR, 'images/')
os.makedirs(IMAGE_DIR, exist_ok=True)


TEMPLATE_IMAGE = 'assets/images/template.png'
TEMPLATE_BURST_IMAGE = 'assets/images/template_bottom.jpg'
# Email screen timeout value in minutes.
EMAIL_SCREEN_TIMEOUT = 2

BURST_IMAGE_MARGIN = 100

INIT_SCREEN = 'home_screen'


SENDGRID_API_KEY = "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"


DEFAULT_CAPTURE_TYPE = 'snap'
DEFAULT_RENDERED_IMAGE = None

try:
    from local_settings import *
except ImportError:
    pass
