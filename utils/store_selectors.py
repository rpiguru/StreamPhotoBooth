from settings import DEFAULT_CAPTURE_TYPE, DEFAULT_RENDERED_IMAGE
from utils.store import StoreGetSetter


class CaptureType(StoreGetSetter):
    key = 'capture_type'
    default_for_missed = DEFAULT_CAPTURE_TYPE


class RenderedImagePath(StoreGetSetter):
    key = 'rendered_image_path'
    default_for_missed = DEFAULT_RENDERED_IMAGE


def clear_store():
    CaptureType().clear()
    RenderedImagePath().clear()
