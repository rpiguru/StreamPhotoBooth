import base64
import ntpath
import os
import re
import validate_email
from sendgrid import SendGridAPIClient, Attachment, FileContent, FileType, FileName, Disposition, ContentId
from sendgrid.helpers.mail import Mail

from settings import SENDGRID_API_KEY
from utils import logger

regex_email = re.compile(r"(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:"
                         r"[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*\""
                         r")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|"
                         r"\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|"
                         r"[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]"
                         r"|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])")


def send_email(from_email=None, to_email=None, subject="Stream Photo Booth", content="Please check attached",
               attach_path=""):
    message = Mail(
        from_email=from_email,
        to_emails=to_email,
        subject=subject,
        html_content=content)
    if os.path.isfile(attach_path):
        with open(attach_path, 'rb') as f:
            data = f.read()
        encoded = base64.b64encode(data).decode()
        attachment = Attachment()
        attachment.file_content = FileContent(encoded)
        attachment.file_type = FileType(f'image/{"gif" if attach_path.endswith("gif") else "jpeg"}')
        attachment.file_name = FileName(ntpath.basename(attach_path))
        attachment.disposition = Disposition('attachment')
        attachment.content_id = ContentId('Content ID')
        message.attachment = attachment
    try:
        sg = SendGridAPIClient(SENDGRID_API_KEY)
        response = sg.send(message)
        logger.debug(f"SendGrid response - {response}")
        return 200 <= response.status_code < 300
    except Exception as e:
        logger.error(f"Failed to send email - {e}")


def is_valid_email(email):
    """
    Validate email format.
    `validate_email` thinks that "aaaaa@bbbbb" is valid one, so use regex before this.
    :param email:
    :return:
    """
    m = regex_email.match(email)
    if m is None:
        return False
    return validate_email.validate_email(email)


if __name__ == '__main__':

    send_email(from_email="from_email@example.com", to_email="shane.carlyon61@gmail.com",
               attach_path="C:\\Users\\Work\/.spb\/images\/snap.jpg")
