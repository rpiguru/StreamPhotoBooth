import cv2

from settings import TEMPLATE_BURST_IMAGE, BURST_IMAGE_MARGIN


def compose_burst_image(frames):
    frames = [cv2.copyMakeBorder(f, int(BURST_IMAGE_MARGIN * .75), int(BURST_IMAGE_MARGIN * .75), BURST_IMAGE_MARGIN,
                                 BURST_IMAGE_MARGIN, cv2.BORDER_CONSTANT, value=[0, 0, 0])
              for f in frames]
    img = cv2.vconcat(frames)
    temp_img = cv2.imread(TEMPLATE_BURST_IMAGE)
    margin = (frames[0].shape[1] - temp_img.shape[1]) // 2
    temp_img = cv2.copyMakeBorder(temp_img, 0, 0, margin, margin, cv2.BORDER_CONSTANT, value=[0, 0, 0])
    img = cv2.vconcat([img, temp_img])
    return img
