import os
import datetime
import platform
import threading
import time

import imageio
import numpy as np
import cv2

from settings import CAMERA_WIDTH, CAMERA_HEIGHT, IMAGE_DIR, GIF_DURATION, TEMPLATE_IMAGE
from utils import logger
from utils.common import is_landscape
from utils.store_selectors import RenderedImagePath


GIF_SKIP_FACTOR = 1
GIF_RESIZE_FACTOR = 0.5


class WebcamStreamer(threading.Thread):

    def __init__(self, video_port):
        super().__init__()
        self._frame = None
        self.video_port = video_port
        self._cap = None
        self._stopped = False
        self._gif_start_time = -1
        self._gif_images = []
        self._gif_cnt = 0
        self._template = None

    def run(self) -> None:
        self._template = cv2.imread(TEMPLATE_IMAGE)
        while not self._stopped:
            if self._cap is not None:
                ret, frame = self._cap.read()
                if ret:
                    if platform.system() == 'Windows':
                        # Remove black side area(left & right) as the windows driver does not support 16:9, why? :)
                        h, w = frame.shape[:2]
                        frame = frame[:, w // 8:w - w // 8]
                    if not is_landscape:
                        frame = np.rot90(frame)
                    self._frame = frame
                    if self._gif_start_time > 0:
                        if time.time() - self._gif_start_time < GIF_DURATION:
                            if self._gif_cnt == GIF_SKIP_FACTOR:
                                if is_landscape:
                                    _img = cv2.hconcat([frame.copy(), self._template])
                                else:
                                    _img = cv2.vconcat([frame.copy(), self._template])
                                h, w, _ = _img.shape
                                _img = cv2.resize(_img, (int(w * GIF_RESIZE_FACTOR), int(h * GIF_RESIZE_FACTOR)))
                                self._gif_images.append(cv2.cvtColor(_img, cv2.COLOR_BGR2RGB))
                                self._gif_cnt = 0
                            else:
                                self._gif_cnt += 1
                        else:
                            self._gif_start_time = -1
                            threading.Thread(target=self._create_gif).start()
                else:
                    logger.error(f"Failed to grab video frame from camera!")
                    self._frame = None
                    self._cap = None
            else:
                self._cap = cv2.VideoCapture(self.video_port)
                self._cap.set(cv2.CAP_PROP_FRAME_WIDTH, CAMERA_WIDTH)
                self._cap.set(cv2.CAP_PROP_FRAME_HEIGHT, CAMERA_HEIGHT)
            time.sleep(.001)

        if self._cap is not None:
            self._cap.release()

    def read(self, template=False):
        if is_landscape:
            return cv2.hconcat([self._frame, self._template]) if template else self._frame
        else:
            return cv2.vconcat([self._frame, self._template]) if template else self._frame

    def create_snap_photo(self, template=True):
        file_name = os.path.join(IMAGE_DIR, f"{datetime.datetime.now().isoformat().replace(':', '-')}.jpg")
        if self._frame is not None:
            if cv2.imwrite(file_name, self.read(template=template)):
                logger.info(f"Captured to a file - {file_name}")
                return file_name
        else:
            logger.warning("Cannot take photo")

    def compose_gif(self):
        self._gif_start_time = time.time()

    def _create_gif(self):
        gif_file = os.path.join(IMAGE_DIR, f"{datetime.datetime.now().isoformat().replace(':', '-')}.mp4")
        imageio.mimsave(gif_file, self._gif_images * 2)
        logger.info(f"Composed a gif file - {gif_file}")
        self._gif_images = []
        RenderedImagePath().set(gif_file)

    def stop(self):
        self._stopped = True
