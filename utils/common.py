"""

    Common utilities

"""
import json
import os
import shutil

from settings import CONFIG_FILE, DEFAULT_WIDTH, DEFAULT_HEIGHT

_cur_dir = os.path.dirname(os.path.realpath(__file__))


def get_human_file_size(byte_size, units=None):
    if units is None:
        units = [' bytes', 'KB', 'MB', 'GB']
    return str(byte_size) + units[0] if byte_size < 1024 else get_human_file_size(byte_size >> 10, units[1:])


if not os.path.isfile(CONFIG_FILE):
    shutil.copy(os.path.join(_cur_dir, os.pardir, 'default_config.json'), CONFIG_FILE)


def get_config():
    return json.loads(open(CONFIG_FILE).read())


is_landscape = get_config().get("mode") == 'LANDSCAPE'

WIDTH = DEFAULT_WIDTH if is_landscape else DEFAULT_HEIGHT
HEIGHT = DEFAULT_HEIGHT if is_landscape else DEFAULT_WIDTH


def update_config(data):
    old_data = get_config()
    old_data.update(**data)
    with open(CONFIG_FILE, 'w+') as outfile:
        json.dump(old_data, outfile, indent=True)
