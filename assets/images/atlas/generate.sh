#!/bin/sh

rm -rf defaulttheme*
# with virtualenv activated
python3 -m kivy.atlas defaulttheme 512x512 *.png

cp -v defaulttheme* ../assets/images/
