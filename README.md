# Stream Photo Booth Application

## Installation

### Install Python3.7+ on Windows.

https://www.python.org/downloads/windows/


### Install Kivy on Windows

```bash
python -m pip install -U pip wheel setuptools
python -m pip install docutils pygments pypiwin32 kivy_deps.sdl2==0.1.* kivy_deps.glew==0.1.*
python -m pip install kivy_deps.gstreamer==0.1.*
python -m pip install kivy_deps.angle==0.1.*
python -m pip install kivy==1.11.1
```

### Clone this repository and install dependencies

```bash
git clone https://gitlab.com/rpiguru/StreamPhotoBooth
cd StreamPhotoBooth
python -m pip install -r requirements.txt
```

### And launch it!

```bash
python main.py
```

## Packaging

- Change directory to `StreamPhotoBooth`.
- Open **Power Shell** by right-clicking the blank space with `Shift` key pressed.
- Type this on cmd:
```bash
python -m PyInstaller spb.spec
```
- Packaged result is in `dist` directory. So just copy and paste it to where you want to put, and enjoy! :)
