# -*- mode: python -*-
from kivy_deps import sdl2, glew

block_cipher = None


a = Analysis(['main.py'],
             pathex=['F:\\workspace\\StreamPhotoBooth'],
             binaries=[],
             datas=[
                 ('dll\\opencv_ffmpeg345_64.dll', '.'),
                 ('dll\\api-ms-win-downlevel-shlwapi-l1-1-0.dll', '.'),
             ],
             hiddenimports=[
                 'win32timezone',
                 'enchant'
             ],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)

pyz = PYZ(a.pure, a.zipped_data, cipher=block_cipher)

exe = EXE(pyz,
          a.scripts,
          [],
          exclude_binaries=True,
          name='StreamPhotoBooth',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          console=True)

coll = COLLECT(exe, Tree('.\\'),
               a.binaries,
               a.zipfiles,
               a.datas,
               *[Tree(p) for p in (sdl2.dep_bins + glew.dep_bins)],
               strip=False,
               upx=True,
               name='StreamPhotoBooth')
