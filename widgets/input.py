from kivy.animation import Animation
from kivy.properties import StringProperty, BooleanProperty, NumericProperty
from kivy.uix.textinput import TextInput
from kivy.core.window import Window
from kivymd.textfields import MDTextField
from widgets.base import DefaultInput


class SPBMDTextField(MDTextField, DefaultInput):

    write_tab = BooleanProperty(False)

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.helper_text_mode = 'on_error'

    def mark_as_normal(self):
        self.error = False

    def mark_as_error(self):
        self.error = True
        Animation(duration=.2, _current_error_color=self.error_color).start(self)

    def on_touch_down(self, touch):
        super(SPBMDTextField, self).on_touch_down(touch)
        self.error = False

    def _get_value(self):
        return self.text

    def custom_error_check(self):
        pass

    def set_value(self, value):
        self.text = value


class SPBTextInput(TextInput, DefaultInput):

    background_normal = StringProperty('assets/images/atlas/textinput.png')

    background_disabled_normal = StringProperty('assets/images/atlas/textinput_disabled.png')

    capitalized = BooleanProperty(False)

    switch_on_enter = BooleanProperty(True)

    rounded = BooleanProperty(False)

    def __init__(self, **kwargs):
        super(SPBTextInput, self).__init__(**kwargs)

    def on_rounded(self, *args):
        if self.rounded:
            self.background_normal = 'assets/images/atlas/textinput_rounded.png'
            self.background_disabled_normal = 'assets/images/atlas/textinput_rounded_disabled.png'
        else:
            self.background_normal = 'assets/images/atlas/textinput.png'
            self.background_disabled_normal = 'assets/images/atlas/textinput_disabled.png'

    def on_text_validate(self):
        if self.switch_on_enter:
            next_widget = self._get_focus_next('focus_next')
            if next_widget:
                next_widget.focus = True

    def _get_value(self):
        return self.text

    def custom_error_check(self):
        pass

    def set_value(self, value):
        if value is None:
            value = ''
        self.text = value

    def mark_as_error(self):
        if self.rounded:
            self.background_normal = 'assets/images/atlas/textinput_rounded_error.png'
        else:
            self.background_normal = 'assets/images/atlas/textinput_error.png'

    def mark_as_normal(self):
        if self.rounded:
            self.background_normal = 'assets/images/atlas/textinput_rounded.png'
        else:
            self.background_normal = 'assets/images/atlas/textinput.png'

    def on_touch_down(self, touch):
        super(SPBTextInput, self).on_touch_down(touch)
        if not self.disabled:
            if self.collide_point(touch.x, touch.y):
                self.clear_errors()
            else:
                self.focus = False


class SPBEmailTextInput(TextInput):

    multiline = BooleanProperty(False)
    font_size = NumericProperty(30)
    background_normal = StringProperty("assets/images/transparent.png")
    background_active = StringProperty("assets/images/transparent.png")

    def __init__(self, **kwargs):
        super(SPBEmailTextInput, self).__init__(**kwargs)
        # self._keyboard = Window.request_keyboard(self._keyboard_closed, self, 'text')
        # if self._keyboard.widget:
            # If it exists, this widget is a VKeyboard object which you can use
            # to change the keyboard layout.
            # pass

    def _keyboard_closed(self):
        self._keyboard.unbind(on_key_down=self._on_keyboard_down)
        self._keyboard = None
