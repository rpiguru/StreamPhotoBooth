import os

from kivy.clock import Clock
from kivy.lang import Builder
from kivy.properties import OptionProperty, StringProperty, ListProperty, NumericProperty, ObjectProperty, \
    BooleanProperty
from kivy.uix.modalview import ModalView
from kivy.uix.popup import Popup

from kivymd.dialog import MDDialog
from kivymd.theming import ThemableBehavior
from utils.common import get_config, update_config
from widgets.snackbar import show_error_snackbar


Builder.load_file('widgets/kv/dialog.kv')


class SPBModalView(ThemableBehavior, ModalView):
    pass


class FileChooserDialog(Popup):
    mode = OptionProperty('file', options=['file', 'dir'])
    extensions = ListProperty()
    path = StringProperty('')

    def __init__(self, **kwargs):
        super(FileChooserDialog, self).__init__(**kwargs)
        self.register_event_type('on_confirm')
        self.path = get_config().get('last_dir', '')
        if self.path is None or not self.path:
            self.path = os.path.expanduser('~/')

    def filter_content(self, directory, filename):
        if self.mode == 'file':
            if self.extensions:
                return filename.split('.')[-1] in self.extensions
            else:
                return True
        else:
            return os.path.isdir(os.path.join(directory, filename))

    def on_btn_create(self):
        dlg = InputDialog(title="Create New Folder")
        dlg.bind(on_confirm=self._on_create_new_folder)
        dlg.open()

    def _on_create_new_folder(self, *args):
        new_folder = os.path.join(self.ids.txt_path.text, args[1])
        try:
            os.makedirs(new_folder)
            self.path = new_folder
        except Exception as e:
            show_error_snackbar('{}'.format(e))

    def on_ok(self):
        self.dismiss()
        path = self.ids.txt_path.text
        update_config({'last_dir': os.path.dirname(path)})
        self.dispatch('on_confirm', path)

    def on_confirm(self, *args):
        pass

    def selected(self):
        self.ids.txt_path.text = str(self.ids.fc.selection[0])


class MessageBox(SPBModalView):
    msg = StringProperty()


class LoadingDialog(ModalView):
    pass


class NotificationDialog(MDDialog):

    message = StringProperty('')

    def __init__(self, **kwargs):
        super(NotificationDialog, self).__init__(**kwargs)


class ProgressDialog(ThemableBehavior, ModalView):

    title = StringProperty()
    value = NumericProperty()

    def update_progress(self, title="", value=0):
        self.title = title
        self.value = value


class InputDialog(MDDialog):

    text = StringProperty('')
    hint_text = StringProperty('')
    input_filter = ObjectProperty(None)

    def __init__(self, **kwargs):
        self.register_event_type('on_confirm')
        super(InputDialog, self).__init__(**kwargs)
        Clock.schedule_once(self.enable_focus, .3)

    def on_yes(self):
        new_val = self.ids.input.text
        if not new_val:
            self.ids.input.mark_as_error()
            return 
        self.dismiss()
        self.dispatch('on_confirm', new_val)

    def on_confirm(self, *args):
        pass

    def enable_focus(self, *args):
        self.ids.input.focus = True


class SelectDialog(MDDialog):

    values = ListProperty()

    def __init__(self, **kwargs):
        super(SelectDialog, self).__init__(**kwargs)
        self.register_event_type('on_confirm')

    def on_open(self):
        self.ids.sp_values.values = self.values

    def on_yes(self):
        new_val = self.ids.sp_values.text
        self.dismiss()
        self.dispatch('on_confirm', new_val)

    def on_confirm(self, *args):
        pass



class YesNoDialog(MDDialog):

    answer = BooleanProperty()

    def __init__(self, **kwargs):
        super(YesNoDialog, self).__init__(**kwargs)

    def on_btn_yes(self):
        self.answer = True
        self.dismiss()

    def on_btn_no(self):
        self.answer = False
        self.dismiss()
