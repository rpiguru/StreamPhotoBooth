import threading
import time
import numpy as np
import cv2
from kivy.app import App
from kivy.clock import Clock
from kivy.properties import BooleanProperty, StringProperty
from kivy.uix.image import Image
from kivy.graphics.texture import Texture

from settings import DARK_SCALE, BLUR_SIZE


def frame_to_buf(frame):
    """
    Convert OpenCV image frame to the texture that can be used in Image widget.
    :param frame:
    :return:
    """
    if frame is None:
        return
    buf1 = cv2.flip(frame, 0)
    if buf1 is not None:
        buf = buf1.tostring()
        texture = Texture.create(size=(frame.shape[1], frame.shape[0]), colorfmt='bgr')
        texture.blit_buffer(buf, colorfmt='bgr', bufferfmt='ubyte')
        return texture


class VideoWidget(Image):
    """:class:`VideoWidget` is an implementation of the video live feed.

    Call `start()` function to start live video feed on this widget,
    and call `pause()` function to pause.

    Attention: Make sure to call `stop()` function before moving to other screen!!!
               e.g. If this widget is used in a `Screen` widget, make use to
               add `stop()` function in its `on_leave()` function!
    """

    _event_take_video = None

    vertical_flip = BooleanProperty(False)
    """:attr:`horizontal_flip` is an :class:`~kivy.properties.BooleanProperty` and
        defaults to False.
    """

    horizontal_flip = BooleanProperty(True)
    """:attr:`horizontal_flip` is an :class:`~kivy.properties.BooleanProperty` and
    defaults to False.
    """

    is_blur = BooleanProperty(False)

    is_running = BooleanProperty(False)

    _frame = None

    def __init__(self, **kwargs):
        super(VideoWidget, self).__init__(**kwargs)

    def start(self):
        """
        Start live video feed
        :return:
        """
        if self.is_running:
            return

        if self._event_take_video is None:
            self._event_take_video = Clock.schedule_interval(lambda dt: self._take_video(), 1.0 / 30.0)
        else:
            self._event_take_video()
        self.is_running = True

    def _take_video(self):
        """
        Capture video frame and update image widget
        :return:
        """
        _frame = App.get_running_app().get_video_frame()
        if _frame is not None:
            _frame = cv2.resize(_frame, (int(self.width), int(self.height)))
            if self.horizontal_flip:
                _frame = cv2.flip(_frame, 1)
            if self.vertical_flip:
                _frame = cv2.flip(_frame, 0)
            if self.is_blur:
                _frame = cv2.blur((_frame * DARK_SCALE).astype(np.uint8), (BLUR_SIZE, BLUR_SIZE), cv2.BORDER_DEFAULT)
            self._frame = _frame
            texture = frame_to_buf(frame=_frame)
            self.texture = texture
        else:
            self.source = 'assets/images/bad_camera.png'
        time.sleep(.01)

    def get_frame(self):
        return self._frame

    def get_raw_frame(self):
        return self._raw_frame

    def stop(self):
        if self._event_take_video and self._event_take_video.is_triggered:
            self._event_take_video.cancel()
            self._event_take_video = None
        self.is_running = False


class VideoFileWidget(Image):

    path = StringProperty()

    def __init__(self, **kwargs):
        super(VideoFileWidget, self).__init__(**kwargs)
        self._b_stop = threading.Event()
        self._b_stop.clear()

    def on_path(self, *args):
        threading.Thread(target=self._read_file).start()

    def _read_file(self):
        cap = cv2.VideoCapture(self.path)
        cnt = 0
        while not self._b_stop.is_set():
            s_time = time.time()
            ret, frame = cap.read()
            cnt += 1
            if cnt == cap.get(cv2.CAP_PROP_FRAME_COUNT):
                cnt = 0
                cap.set(cv2.CAP_PROP_POS_FRAMES, 0)
            Clock.schedule_once(lambda dt: self._update_frame(frame))
            time.sleep(max(0., 1 / 30. - time.time() + s_time))

        cap.release()

    def _update_frame(self, frame):
        texture = frame_to_buf(frame=frame)
        self.texture = texture

    def stop(self):
        self._b_stop.set()
