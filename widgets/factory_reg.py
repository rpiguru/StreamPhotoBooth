from kivy.factory import Factory

from kivymd.button import MDRaisedButton, MDFlatButton, MDIconButton
from kivymd.card import MDCard, MDSeparator
from kivymd.progressbar import MDProgressBar
from kivymd.selectioncontrols import MDCheckbox
from kivymd.spinner import MDSpinner
from kivymd.textfields import MDTextField
from widgets.button import SPBButton, ImageButton
from widgets.checkbox import SPBMDCheckbox
from widgets.input import SPBTextInput, SPBMDTextField, SPBEmailTextInput
from widgets.label import ScrollableLabel, ColoredLabel, IconLabel
from widgets.spinner import SPBSpinner
from widgets.video import VideoWidget

Factory.register('ScrollableLabel', cls=ScrollableLabel)
Factory.register('ColoredLabel', cls=ColoredLabel)
Factory.register('MDCard', cls=MDCard)
Factory.register('SPBButton', cls=SPBButton)
Factory.register('ImageButton', cls=ImageButton)
Factory.register('MDRaisedButton', cls=MDRaisedButton)
Factory.register('MDFlatButton', cls=MDFlatButton)
Factory.register('MDIconButton', cls=MDIconButton)
Factory.register('MDSpinner', cls=MDSpinner)
Factory.register('MDTextField', cls=MDTextField)
Factory.register('IconLabel', cls=IconLabel)
Factory.register('MDCheckbox', cls=MDCheckbox)
Factory.register('SPBTextInput', cls=SPBTextInput)
Factory.register('SPBMDTextField', cls=SPBMDTextField)
Factory.register('SPBMDCheckbox', cls=SPBMDCheckbox)
Factory.register('MDSeparator', cls=MDSeparator)
Factory.register('MDProgressBar', cls=MDProgressBar)
Factory.register('SPBSpinner', cls=SPBSpinner)
Factory.register('VideoWidget', cls=VideoWidget)
Factory.register('SPBEmailTextInput', cls=SPBEmailTextInput)
