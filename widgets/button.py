from kivy.uix.behaviors import ButtonBehavior
from kivy.uix.image import Image

from assets.styles import defaultstyle
from kivy.properties import OptionProperty, NumericProperty, StringProperty, BooleanProperty
from kivy.uix.button import Button


class SPBButton(Button):
    button_type = OptionProperty('blue', options=['blue', 'white'])
    font_size = NumericProperty(17)

    def __init__(self, **kwargs):
        super(SPBButton, self).__init__(**kwargs)
        self.set_default_color()

    def set_white_color(self):
        self.color = defaultstyle.COLOR_1
        self.background_normal = 'assets/images/buttons/white_button.png'
        self.background_down = 'assets/images/buttons/white_button_pressed.png'

    def set_default_color(self):
        self.color = (1, 1, 1, 1)
        self.background_normal = 'assets/images/buttons/blue_button.png'
        self.background_down = 'assets/images/buttons/blue_button_pressed.png'

    def on_button_type(self, obj, val):
        if val == 'blue':
            obj.set_default_color()
        else:
            self.set_white_color()


class ImageButton(ButtonBehavior, Image):

    background_normal = StringProperty()
    background_down = StringProperty()
    is_dynamic = BooleanProperty(True)

    def on_background_normal(self, *args):
        self.on_state()

    def on_state(self, *args):
        if self.is_dynamic:
            self.source = self.background_normal if self.state == 'normal' else self.background_down
