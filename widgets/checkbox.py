from kivymd.selectioncontrols import MDCheckbox
from widgets.base import DefaultInput


class SPBMDCheckbox(MDCheckbox, DefaultInput):

    def mark_as_normal(self):
        pass

    def mark_as_error(self):
        pass

    def set_value(self, value):
        self.active = value

    def _get_value(self):
        return self.active

    def custom_error_check(self):
        pass
